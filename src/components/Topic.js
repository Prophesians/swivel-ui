import React from 'react'
import timestamp from 'unix-timestamp'

const postWrapperPost = {
    marginLeft: '20px',
    marginRight: '20px',
    paddingTop: '20px',
    paddingBottom: '15px',
    borderBottom: '1px solid #e8e8e8',

};
const h3 = {
    color: '#303030',
    fontWeight: '400',
    fontFamily: 'Cera Pro,Helvetica Neue,Helvetica,Arial,sans-serif',
};

const hrefStyle = {
    textDecoration: 'none',
    cursor: 'pointer'
};

const dateStyle = {
    color: '#999',
};

const authorStyle = {
    float: 'right',
    color: '#999',
};

export default ({Title, Url, By, Time, Tags}) =>

    <React.Fragment>
        <a href={Url} target="blank" id="article-link" style={hrefStyle}>
            <div className="post-wrapper__post" style={postWrapperPost}>
                    <div className="post-wrapper__post__date" style={dateStyle}>
                        <span id="article-date">{timestamp.toDate(Time).toDateString()}</span>
                        <span  id="article-author" style={authorStyle}>By: {By}</span>
                        <span><h3  id="article-title" style={h3}>{Title}</h3></span>
                    </div>
            </div>
        </a>
    </ React.Fragment>
    