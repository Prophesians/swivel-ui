import { setTopics, failure, fetchData } from "../../src/actions";
import Axios from 'axios';  
import MockAdapter from 'axios-mock-adapter'
import td from 'testdouble'

const mockAxios = new MockAdapter(Axios);


describe('actions', ()=>{
    describe('failure',()=>{
        it('should return an action with type API_FAILURE',()=>{
            expect(failure()).toEqual({
                type: 'swivelui/API_FAILURE',
                error: 'SOMETHING WENT WRONG!!'
            })
        })
    });
    describe('setTopics',()=>{
        it('should return an action with type SET_TOPICS', ()=>{
            expect(setTopics([])).toEqual({
                type: 'swivelui/SET_TOPICS',
                topics: []
            })
        })
    });
    describe('fetchData',()=>{
        it('should dispatch setTopics when the API call is success', (done)=>{
            const dispatch = td.function('dispatch');

            mockAxios.onGet('http://localhost:8080/api/1/articles').reply(200, {Topics: []});

            fetchData(dispatch, 1).then(() => {
                td.verify(dispatch(setTopics([])));
                done()
            })
        });

        it('should dispatch failure when the API call is failed', (done)=>{
            const dispatch = td.function('dispatch');

            mockAxios.onGet('http://localhost:8080/api/1/articles').reply(500);

            fetchData(dispatch, 1).then(() => {
                td.verify(dispatch(failure()));
                done()
            })
        })
    })
});