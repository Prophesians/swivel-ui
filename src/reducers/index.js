import { SET_TOPICS, API_FAILURE } from "../utils/constants";

export default (state={topics:[]}, action)=>{
    const stateFor = {
        [SET_TOPICS]: ()=>{
            return {...state, topics: action.topics}
        },
        [API_FAILURE]: ()=>{
            return {...state, error: action.error}
        }
    }

    return stateFor[action.type] ? stateFor[action.type](): state
}