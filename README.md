# Swivel UI
A web Application for Swivel

## Getting Started

    
### Prerequisites

Install [nodejs](https://nodejs.org/en/download/)

### Developer Setup

Clone the repository to make a local copy of the project

```
git clone https://gitlab.com/Prophesians/swivel-ui.git
```

Install Dependencies

```
npm install
```

Run application locally on `localhost:3000`

```
npm start
```

## Running the tests

```
npm  test
``` 