import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './containers/Home';
import Articles from './containers/Articles';

const Routes = () => (
    <Switch>
        <Route exact path="/" component={Home} />
            <Route exact path="/:userID/articles" component={Articles} />
    </Switch>
);

export default Routes;
