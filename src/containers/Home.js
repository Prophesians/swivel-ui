import React from "react";
import Header from "../components/Header.jsx"
import Footer from "../components/Footer.jsx"

class HomePage extends React.Component {
    render() {
        return (
            <div className="app">
                <Header/>
                <h2>
                    Welcome to Swivel
                </h2>
                <h3>
                    Read more, Learn more, Grow more ...!
                </h3>
                <Footer/>
            </div>
        );
    }
}

export default HomePage;