import Topic from "../components/Topic";
import RenderMany from "../components/RenderMany"

import React from 'react';
import {fetchData} from "../actions";
import {connect} from "react-redux";
import Header from "../components/Header.jsx"
import Footer from "../components/Footer.jsx"

const postWrapperStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    width: '950px',
    margin: '0 auto'
};

class DumbApp extends React.Component {
    componentWillMount() {
        const userID = this.props.match.params.userID;

        this.props.fetchData(userID);
    }

    render() {
        return (
            <div className="app">
                <Header/>
                <React.Fragment>
                    <div className="post-wrapper" style={postWrapperStyle}>
                        <RenderMany items={this.props.topics} Component={Topic}/>
                    </div>
                </ React.Fragment>
                <Footer/>
            </div>

        )
    }
}

const App = (props) => (<DumbApp {...props}/>);


const mapStateToProps = (state) => ({
    topics: state.topics,
});

const mapDispatchToProps = (dispatch) => ({
    fetchData: (userID) => fetchData(dispatch, userID)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

