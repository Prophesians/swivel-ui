import React from 'react'
import {
    BrowserRouter,
} from 'react-router-dom';

import Routes from './Routes';

class DumbApp extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        )
    }
}

export default DumbApp