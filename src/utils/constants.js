export const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || "http://localhost:8080/api";
export const ARTICLE_API = 'articles';

export const SET_TOPICS = 'swivelui/SET_TOPICS';
export const API_FAILURE = 'swivelui/API_FAILURE';
export const NO_OF_ARTICLES_PER_PAGE = 20;
