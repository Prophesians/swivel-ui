import Axios from "axios";
import {API_ENDPOINT, ARTICLE_API, SET_TOPICS, API_FAILURE, NO_OF_ARTICLES} from '../utils/constants'


export const setTopics = (topics) =>({
    type: SET_TOPICS,
    topics
});

export const failure = () => ({
    type: API_FAILURE,
    error: 'SOMETHING WENT WRONG!!'
});

export const fetchData = (dispatch, userID) => {
    const url =  `${API_ENDPOINT}/${userID}/${ARTICLE_API}/${NO_OF_ARTICLES}`;
    return Axios.get(url).then(({data})=>{
        dispatch(setTopics(data.Topics))
    }).catch(_=>
            dispatch(failure())
        )
};
