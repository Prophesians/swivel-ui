import Topic from '../../src/components/Topic'
import {mount} from 'enzyme'
import React from 'react';

describe('Topic', ()=>{
    it('should render a article with given details', ()=>{
        const topic = mount(<Topic Title="Title" Url="/abc" By="by" Time={1231231231} />);
        const article = topic.find('#article-link');
        const header = topic.find('#article-title');
        const author = topic.find('#article-author');
        const link = topic.find('#article-date');


        expect(article.exists()).toBe(true);
        expect(header.text()).toBe('Title');
        expect(author.text()).toBe('By: by');
        expect(link.text()).toBe('Tue Jan 06 2009');
    })
});