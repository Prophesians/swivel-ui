import React from 'react';

const containerClass = {
    backgroundColor: '#ffffff',
    width: '100%',
    padding: '2%',
};

const headerStyle = {
    marginLeft: '20px',
    marginRight: '20px',
    paddingTop: '20px',
    paddingBottom: '15px',
    borderBottom: '1px solid #e8e8e8',
    color: '#484848',
    fontFamily: 'Cera Pro,Helvetica Neue,Helvetica,Arial,sans-serif',
};

export default ({Component, items}) =>

    <div style={containerClass}>
        <React.Fragment>
            <div style={headerStyle}>
                <h2>Articles you will like to read</h2>
            </div>
            {
                items.map((item, index) =>
                    <Component key={`item-${index}`} {...item}/>)
            }
        </ React.Fragment>
    </div>


